#ifndef BTSTACK_SYNC_H_
#define BTSTACK_SYNC_H_

#include <pthread.h>
#include <stdbool.h>
#include <sys/select.h>

#ifdef __cplusplus
extern "C" {
#endif

void btstack_sync_pipe_init();
int btstack_sync_set_pipe(fd_set* readDescriptor);
void btstack_sync_check_pipe(fd_set* readDescriptor);

void btstack_sync_write_pipe();

void btstack_sync_mutex_init();
void btstack_sync_mutex_lock();
void btstack_sync_mutex_unlock();

void btstack_sync_begin_command();
void btstack_sync_end_command();

bool btstack_sync_is_finished();
void btstack_sync_exit();

#ifdef __cplusplus
}
#endif

#endif
