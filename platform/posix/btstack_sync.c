#include "btstack_sync.h"
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

static pthread_mutex_t g_btstack_mutex_sync;
static pthread_mutexattr_t g_btstack_mutex_attr;

static int g_sync_pipe[2];
static bool exit = false;


void btstack_sync_pipe_init(){
	exit = false;
	if(pipe(g_sync_pipe) == 0 )
		dprintf(1,"btstack_sync_pipe Pipe Created\r");	
	else
		dprintf(1,"btstack_sync_pipe Pipe ERROR\r");
}

int btstack_sync_set_pipe(fd_set* readDescriptor){	
	FD_SET(g_sync_pipe[0], readDescriptor);
	if(g_sync_pipe[0]<0)
		dprintf(1,"btstack_sync_set_pipe ERROR");

	return g_sync_pipe[0];
}

void btstack_sync_check_pipe(fd_set* readDescriptor){
	if(FD_ISSET(g_sync_pipe[0], readDescriptor)){
		unsigned char byte;
		int bytesRead = read(g_sync_pipe[0],&byte,1);
		if(bytesRead<0)
			dprintf(1,"btstack_sync_pipe read ERROR\r");		
	}
}

void btstack_sync_write_pipe(){
	const unsigned char byte = 0x01;
	int bytes_written = (int) write(g_sync_pipe[1], &byte, 1);
	if(bytes_written<=0)
		dprintf(1,"btstack_sync_pipe write ERROR\r");
}

void btstack_sync_mutex_init(){
	pthread_mutexattr_init(&g_btstack_mutex_attr);
	pthread_mutexattr_settype(&g_btstack_mutex_attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&g_btstack_mutex_sync,&g_btstack_mutex_attr);
}

void btstack_sync_mutex_lock(){
	pthread_mutex_lock(&g_btstack_mutex_sync);
}

void btstack_sync_mutex_unlock(){
	pthread_mutex_unlock(&g_btstack_mutex_sync);
}

void btstack_sync_begin_command(){
	btstack_sync_mutex_lock();
}

void btstack_sync_end_command(){
	btstack_sync_write_pipe();
	btstack_sync_mutex_unlock();
}

void btstack_sync_exit()
{
	exit = true;
}

bool btstack_sync_is_finished()
{
	return exit;
}
